wget https://cdn.ipip.net/17mon/country.zip # 下载ip库 然后unzip 解压，得到country.txt
ipset create cnlist hash:net # 创建 ipset 
#开始添加ip 到ipset 里面
awk '$2 ~ /CN/{print "ipset add cnlist " $1}' /mnt/disk0/tizi/country.txt  | bash
ipset add cnlist 192.168.0.0/16
ipset add cnlist 127.0.0.0/8
ipset add cnlist 172.16.0.0/12
ipset add cnlist 10.0.0.0/8
# 添加完毕 接下来搞iptables 
iptables -t nat -N ss # 创建一个chain 在nat表上，名字是ss
iptables -t nat -A ss -m set --match-set cnlist dst -j RETURN # 如果目的ip 是中国ip，返回
iptables -t nat -A ss -m mark —mark 1989 -j RETURN # 匹配到1989 mark 的流量，返回
iptables -t nat -A ss -p tcp  -j REDIRECT --to-ports 1089 #  把tcp 流量转发到1089 tproxy 端口,这个端口号可以根据配置改
iptables -t nat -A OUTPUT -j ss # OUTPUT 添加跳转到ss 的规则
